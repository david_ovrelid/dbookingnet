﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace dbooking.Models
{
    public class Room
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string RoomName { get; set; }

        [Required]
        public bool IsRoomClean { get; set; }

        [Required]
        public RoomType RoomType { get; set; }
    }
}