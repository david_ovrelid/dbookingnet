﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace dbooking.Models
{
    public class RoomType
    {
        public int Id { get; set; }
        
        [Required]
        public string Roomtype { get; set; }

        [Required]
        public int NumberOfBeds { get; set; }

        [Required]
        public decimal PricePerNightAdult { get; set; }

        [Required]
        public int NumberOfBedsForKids { get; set; }

        [Required]
        public decimal PricePerNightKid { get; set; }

        [Required]
        public decimal PricePerAdultWeekend { get; set; }

        [Required]
        public decimal PricePerKidWeekend { get; set; }
    }
}