﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(dbooking.Startup))]
namespace dbooking
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
