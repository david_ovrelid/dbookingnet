namespace dbooking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoomTypeMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoomTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Roomtype = c.String(),
                        NumberOfBeds = c.Int(nullable: false),
                        PricePerNightAdult = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NumberOfBedsForKids = c.Int(nullable: false),
                        PricePerNightKid = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PricePerAdultWeekend = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PricePerKidWeekend = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RoomTypes");
        }
    }
}
