namespace dbooking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequiredDataAnotationsToRoom : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rooms", "RoomType_Id", "dbo.RoomTypes");
            DropIndex("dbo.Rooms", new[] { "RoomType_Id" });
            AlterColumn("dbo.Rooms", "RoomName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Rooms", "RoomType_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Rooms", "RoomType_Id");
            AddForeignKey("dbo.Rooms", "RoomType_Id", "dbo.RoomTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "RoomType_Id", "dbo.RoomTypes");
            DropIndex("dbo.Rooms", new[] { "RoomType_Id" });
            AlterColumn("dbo.Rooms", "RoomType_Id", c => c.Int());
            AlterColumn("dbo.Rooms", "RoomName", c => c.String());
            CreateIndex("dbo.Rooms", "RoomType_Id");
            AddForeignKey("dbo.Rooms", "RoomType_Id", "dbo.RoomTypes", "Id");
        }
    }
}
