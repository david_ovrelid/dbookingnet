namespace dbooking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequiredToRoomTypes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RoomTypes", "Roomtype", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RoomTypes", "Roomtype", c => c.String());
        }
    }
}
